angular.module('app',['ngRoute', 'ngResource'])
    .config(function ($routeProvider, $httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $routeProvider
            .when('/calculator',{
                templateUrl: 'partials/calculator.html',
                controller: 'CalcController',
                controllerAs: 'calc'
            })
            .when('/interesting',{
                templateUrl: 'partials/interesting.html'
            })
            .when('/films',{
                templateUrl: 'partials/films.html',
                controller: 'ListController',
                controllerAs: 'listCtrl'
            })
            .when('/details/:id',{
                templateUrl: 'partials/details.html',
                controller: 'DetailsController',
                controllerAs: 'detailsCtrl'
            })
            .when('/add',{
                templateUrl: 'partials/addFilm.html',
                controller: 'NewController',
                controllerAs: 'newCtrl'
            })
            .when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'AuthenticationController',
                controllerAs: 'autController'
            })
            .otherwise({
                redirectTo: '/calculator'
            })
    })
    .constant('LOGIN_ENDPOINT', '/login')
    .constant('FILM_ENDPOINT', '/api/films/:id')
    .factory('Film',function ($resource, FILM_ENDPOINT) {
        return $resource(FILM_ENDPOINT);
    })
    .service('Films',function (Film) {
        this.getAll = function () {
            return Film.query();
        };
        this.get = function (index) {
            return Film.get({id: index});
        };
        this.addFilm = function (film) {
            film.$save();
        };
        this.deleteFilm = function () {
            return Film.delete();
        }
    })
    .service('AuthenticationService',function ($http, LOGIN_ENDPOINT) {
        this.authenticate = function (credentials, successCallback) {
            var authHeader = {Authorization: 'Basic ' + btoa(credentials.username+':'+credentials.password)};
            var config = {headers : authHeader};
            $http
                .post(LOGIN_ENDPOINT,{},config)
                .then(function success() {
                    $http.defaults.headers.post.Authorization = authHeader.Authorization;
                    successCallback();
                }, function error(reason) {
                    console.log('Login error');
                    console.log(reason);
                });
        }
        this.logout = function (successCallback) {
            delete $http.defaults.headers.post.Authorization;
            successCallback();
        }
    })
    .controller('ListController',function (Films) {
        var vm = this;
        vm.films = Films.getAll();
        vm.delete = function () {
           vm.films = Films.deleteFilm();
        }
    })
    .controller('DetailsController',function ($routeParams, Films) {
        var vm = this;
        var filmIndex = $routeParams.id;
        vm.film = Films.get(filmIndex);
    })
    .controller('NewController',function (Films, Film) {
        var vm = this;
        vm.film = new Film();
        vm.addFilm = function () {
            Films.addFilm(vm.film);
            vm.film = new Film();
        }
    })
    .controller('AuthenticationController',function ($rootScope, $location, AuthenticationService) {
        var vm = this;
        vm.credentials = {};
        var loginSuccess = function () {
            $rootScope.authenticated = true;
            $location.path('/add');
        }
        vm.login = function () {
            AuthenticationService.authenticate(vm.credentials, loginSuccess);
        }

        var logoutSuccess = function () {
            $rootScope.authenticated = false;
            $location.path('/');
        }

        vm.logout = function () {
            AuthenticationService.logout(logoutSuccess);
        }
    })



// Kontroller odpowiedzialny za dzialanie kalkulatora

    .controller('CalcController',function () {
        this.output = "0";
        this.sign = "";
        this.pass = true;
        
        this.UpdateOutput = function (number) {
            if(this.pass){
                if(this.output == "0"){
                    this.output = String(number);
                }else {
                    this.output += String(number);
                }
            }
        };

        this.add = function () {
            this.pass = true;
            if(this.sign == "+"){
                this.wynik();
                this.currentNumber =  Number(this.output);
                this.output = "0";
            }else {
                this.currentNumber =  Number(this.output);
                this.output = "0";
                this.sign = "+";
            }
        };

        this.subtract = function () {
            this.pass = true;
            this.currentNumber =  Number(this.output);
            this.output = "0";
            this.sign = "-";
        };

        this.division = function () {
            this.pass = true;
            this.currentNumber =  Number(this.output);
            this.output = "0";
            this.sign = "/";
        };

        this.multiple = function () {
            this.pass = true;
            this.currentNumber = Number(this.output);
            this.output = "0";
            this.sign = "*";
        };

        this.clear = function () {
            this.output = "0";
            this.currentNumber = "0";
            this.pass = true;
        };

        this.wynik = function () {
            switch (this.sign){
                case "-":
                    this.output = this.currentNumber - Number(this.output);
                    this.pass = false;
                    break;
                case "+":
                    this.output = this.currentNumber + Number(this.output);
                    this.pass = false;
                    break;
                case "/":
                    if(this.output == "0"){
                        this.output = "Nie można dzielić przez zero";
                        this.pass = false;
                        break;
                    }else {
                        this.output = this.currentNumber / Number(this.output);
                        this.pass = false;
                        break;
                    }
                case "*":
                    this.output = this.currentNumber * Number(this.output);
                    this.pass = false;
                    break;
            }
        }
    });
