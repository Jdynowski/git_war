package com.example.repository;

import com.example.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FilmRepository extends JpaRepository<Film,Long> {
}
