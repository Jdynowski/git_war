package com.example.api;

import com.example.model.Film;
import com.example.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/films")
public class FilmEndpoint {

    private FilmRepository filmRepository;

    @Autowired
    public FilmEndpoint(FilmRepository filmRepository){
        this.filmRepository = filmRepository;
    }

    @GetMapping
    public List<Film> allProducts() {
        return filmRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Film> getById(@PathVariable Long id){
        Optional<Film> film = filmRepository.findById(id);
        return film;
    }

    @PostMapping
    public Film saveProduct(@RequestBody Film film) {
        Film newFilm = filmRepository.save(film);
        return newFilm;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteFilm(){
        filmRepository.deleteAll();
    }
}
